//
//  GameBoardViewController.swift
//  Test1
//
//  Created by Vu Dang Tuan on 5/25/15.
//  Copyright (c) 2015 Vu Dang Tuan. All rights reserved.
//

import UIKit

class GameBoardViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    let ModelCGSize = CGSize(width: 64, height: 58)
    var curCGPoint = CGPoint(x: 0, y: 0)
    var MaxW = 0
    var MaxH = 0
    var CheckW = false
    var CheckH = false
    var CurScreenW = 0
    var CurScreenH = 0
    var CurPosition = Point3D(X: 0, Y: 0, Z: 0)
    var CurQuestion = "QuestionLabel"
    let extraheightforCurScreen = 300
    var alignW = 0
    let alignH = 100
    var labeltext = "0"
    var CurDictionary = [Int : Int]()
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return CalculateNumberOfCellInBoard()
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("MyCell", forIndexPath: indexPath) as! MyBasicCell
        //change position of cell
        ChangeCGPointInBoard()
        //change size in cell
        var curframe = CGRect (origin: curCGPoint, size: ModelCGSize)
        cell.frame = curframe
        //change label in cell
        //ChangeLabelText(indexPath.row)
        cell.numberLabel?.text = "99"
        //scale image but this seem not work
        cell.imageBG.contentMode = UIViewContentMode.ScaleAspectFit
        return cell
    }
    
    func collectionView(collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kind: String,
        atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
            //1
            switch kind {
                //2
            case UICollectionElementKindSectionHeader:
                //3
                let headerView =
                collectionView.dequeueReusableSupplementaryViewOfKind(kind,
                    withReuseIdentifier: "BoardHeaderCell",
                    forIndexPath: indexPath)
                    as! GameBoardHeaderView
                headerView.questionLabel.text = CurQuestion
                return headerView
            default:
                //4
                assert(false, "Unexpected element kind")
            }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func CalculateNumberOfCellInBoard() -> Int {
        let screenSize = UIScreen.mainScreen().bounds
        self.CurScreenH = Int(screenSize.height) + extraheightforCurScreen
        self.CurScreenW = Int(screenSize.width)
        //Get maximum cell in height and and maximum cell in width
        var tmp1 = Int(Float(self.CurScreenW) / (Float(ModelCGSize.width) * 1.5))
        if (Float(tmp1) * 1.5 * Float(ModelCGSize.width) + Float(ModelCGSize.width)) < Float(self.CurScreenW)
        {
            self.MaxW = tmp1
            self.CheckW = true
            //find alignW
            alignW = Int(Float(self.CurScreenW) - (Float(tmp1) * 1.5 * Float(ModelCGSize.width) + Float(ModelCGSize.width)))
            alignW = Int(alignW / 2)
        }
        else
        {
            self.MaxW = tmp1 - 1
            self.CheckW = false
            alignW = Int(Float(self.CurScreenW) - (Float(tmp1) * 1.5 * Float(ModelCGSize.width)))
            alignW = Int(alignW / 2)
        }
        tmp1 = Int(Float(self.CurScreenH) / (Float(ModelCGSize.height) * 1.5))
        if (Float(tmp1) * 1.5 * Float(ModelCGSize.height) + Float(ModelCGSize.height)) < Float(self.CurScreenH)
        {
            self.MaxH = tmp1
            self.CheckH = true
        }
        else
        {
            self.MaxH = tmp1 - 1
            self.CheckH = false
        }
        //calculate for number of cell in board
        //because swift run from 0 to X so we must minus 1 to get right number
        var res = self.MaxH * self.MaxW + (self.MaxW - 1)*(self.MaxH - 1) - (self.MaxH % 2)
        if CheckH == false
        {
            res = res + 1
        }
        return res
    }
    
    func ChangeCGPointInBoard() -> Void {
        if self.CurPosition.Z == 1
        {
            if self.CurPosition.X == self.MaxW
            {
                if self.CheckW == true
                {
                    //position not exists
                    //change
                    self.CurPosition.X = 0
                    self.CurPosition.Y = self.CurPosition.Y + 1
                    self.CurPosition.Z = 0
                    //set point
                    var tmp1 = (Float(self.ModelCGSize.height) *  Float(self.CurPosition.Y)) + Float(alignH)
                    self.curCGPoint = CGPoint(x: alignW, y: Int(tmp1))
                    //change position
                    self.CurPosition.Z = 1
                    return
                }
                else
                {
                    //exist
                    //set point
                    var tmp1 = Float(self.CurPosition.X) * Float(self.ModelCGSize.width) * 1.5
                    var tmp2 = Float(self.CurPosition.Y) * Float(self.ModelCGSize.height)
                    tmp1 = tmp1 + Float(self.ModelCGSize.width)*0.75 + Float(alignW)
                    tmp2 = tmp2 + Float(self.ModelCGSize.height)*0.5 + Float(alignH)
                    self.curCGPoint = CGPoint(x: Int(tmp1), y: Int(tmp2))
                    //change position also
                    self.CurPosition.X = 0
                    self.CurPosition.Z = 0
                    self.CurPosition.Y = self.CurPosition.Y + 1
                    return
                }
            }
            else
            {
                //exist
                //set point
                var tmp1 = Float(self.CurPosition.X) * Float(self.ModelCGSize.width) * 1.5
                var tmp2 = Float(self.CurPosition.Y) * Float(self.ModelCGSize.height)
                tmp1 = tmp1 + Float(self.ModelCGSize.width)*0.75 + Float(alignW)
                tmp2 = tmp2 + Float(self.ModelCGSize.height)*0.5 + Float(alignH)
                self.curCGPoint = CGPoint(x: Int(tmp1), y: Int(tmp2))
                self.CurPosition.X = self.CurPosition.X + 1
                self.CurPosition.Z = 0
                return
            }
        }
        else
        {
            //exist
            var tmp1 = Float(self.CurPosition.X) * Float(self.ModelCGSize.width) * 1.5 + Float(alignW)
            var tmp2 = Float(self.CurPosition.Y) * Float(self.ModelCGSize.height) + Float(alignH)
            self.curCGPoint = CGPoint(x: Int(tmp1), y: Int(tmp2))
            //change position
            self.CurPosition.Z = 1
            return
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        
        
        
        return
    }

    //change labetext when draw the board
    func ChangeLabelText(Index: Int) -> Void
    {
        if Index == 0
        {
           GenerateCurDictionary()
        }
        labeltext = "\(CurDictionary[Index])"
    }
    
    func GenerateCurDictionary() -> Void {
        //for easy mode, the answer is in line(for vertical) or nearly a line(for horizontal)
        //for normal mode, the answer always is in diagonal
        //there are many confusing answers round true answer
        //for hard mode, it can be any type of curve
        //there are many seemed logical answers around true answer
        
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class MyBasicCell : UICollectionViewCell {
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var imageBG: UIImageView!
    
}

class GameBoardHeaderView : UICollectionReusableView {
    
    @IBOutlet var questionLabel: UILabel!
}

struct Point3D {
    var X : Int
    var Y : Int
    var Z : Int
}
