//
//  GameGlobal.swift
//  Test1
//
//  Created by Vu Dang Tuan on 5/27/15.
//  Copyright (c) 2015 Vu Dang Tuan. All rights reserved.
//

import Foundation
import UIKit

class GlobalVar {
    //level: 
    //1 : easy
    //2 : normal
    //3 : hard
    static var curLevel = 2
    //Map from 1 -> 400
    static var curMap = 1
    static var curBoardCell = Set<Int>()
    static var curDictionary = MyDictionary(Index: 0, Quiz: [1,4,7,10], Answer: [13,16,19,22,25], Explain: "The formulation of this: 3*i - 2.", Type: 1)
    static var CurMapUnavailable = Set<Int>()
    
    
    static func CountMapDataAndCheck(FileName : String, Extension : String) -> Int {
        var x = MyReaderAndWriterFileInBundle.ReaderAllFile(FileName, Extension: Extension)
        var result = 0
        for line in x.Line {
            var check = CheckLineString(line)
            if check == true
            {
                result = result + 1
            }
            else
            {
                CurMapUnavailable.insert(result)
            }
        }
        return result
    }
    
    static func CheckPosition(Position : Int) -> Int {
        var tmp = Position
        for x in CurMapUnavailable {
            if x < Position
            {
                tmp = tmp + 1
            }
            else
            {
                break
            }
        }
        return tmp
    }
    
    
    //the position here is the position in the available map number
    static func LoadOneDataFromTextFile(FileName : String, Extension : String, Position : Int) -> Void {
        var x = MyReaderAndWriterFileInBundle.ReaderAllFile(FileName, Extension: Extension)
        var position = CheckPosition(Position)
        //add to dictionary
        var tmp = 0
        for line in x.Line {
            if tmp == position
            {
                ParsingLineString(line)
                return
            }
            else
            {
                tmp = tmp + 1
            }
        }
    }
    
    static func ParsingLineString (Line : String) -> Void {
        
        
    }
    
    static func CheckLineString (Line : String) -> Bool {
        var check = true
        var start = Line.startIndex
        var pre = start
        var end = Line.endIndex
        var count = 0
        for ; start < end; start = start.successor()
        {
            if Line[start] == ","
            {
                count = count + 1
                if pre == start
                {
                    check = false
                }
                pre = start
                pre = pre.successor()
            }
            else
            {
                
            }
        }
        return check
    }
    
}

struct MyDictionary
{
    var Index : Int
    var Quiz = Set<Int>()
    var Answer = Set<Int>()
    var Explain : String
    var Type : Int
}
