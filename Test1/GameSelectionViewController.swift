//
//  GameSelectionViewController.swift
//  Test1
//
//  Created by Vu Dang Tuan on 5/24/15.
//  Copyright (c) 2015 Vu Dang Tuan. All rights reserved.
//

import UIKit

class GameSelectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    @IBOutlet var MyConnectionView: UICollectionView!

    let noteLabel = "Choose a level!"
    var curCGPoint = CGPoint(x: 0, y: 0)
    var ModelCGSize = CGSize(width: 60, height: 60)
    var CurScreenW = 0
    var CurScreenH = 0
    let extraheightforCurScreen = 300
    var alignW = 0
    let alignH = 100
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return CalculateNumberOfCellInBoard()
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCellWithReuseIdentifier("GameSelectionID", forIndexPath: indexPath) as! GameSelectionCell
        //change position of cell
        ChangeCGPointInBoard()
        //change size in cell
        var curframe = CGRect (origin: curCGPoint, size: ModelCGSize)
        cell.frame = curframe
        
        cell.numberLabel.text = "\(indexPath.row + 1)"
        
        return cell
    }
    
    func CalculateNumberOfCellInBoard() -> Int {
        //read how many line in datafile.txt
        
        
        return 1
    }
    
    func ChangeCGPointInBoard() -> Void {
        
        
    }
    
    func collectionView(collectionView: UICollectionView,
        viewForSupplementaryElementOfKind kind: String,
        atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
            //1
            switch kind {
                //2
            case UICollectionElementKindSectionHeader:
                //3
                let headerView =
                collectionView.dequeueReusableSupplementaryViewOfKind(kind,
                    withReuseIdentifier: "GameSelectionHeaderID",
                    forIndexPath: indexPath)
                    as! GameSelectionHeaderView
                headerView.noteLabel.text = noteLabel
                return headerView
            default:
                //4
                assert(false, "Unexpected element kind")
            }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        var x = indexPath.row
        GlobalVar.curMap = x
        return
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.MyConnectionView.backgroundColor = UIColor(patternImage: UIImage(named: "gameselectiontheme")!)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class GameSelectionCell : UICollectionViewCell {
    
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var numberLabel: UILabel!
}

class GameSelectionHeaderView : UICollectionReusableView {
    @IBOutlet var noteLabel: UILabel!
    
}
